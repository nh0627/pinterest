Watering🐛🌳
======================

> This app is to record daily logs for your plants🌱 You can use this app also to keep your plants alive!

---

## Features
- Retrieve a list of your plants
- View, add, update and delete details of your plants
- Record the date of watering, potting and feeding of your plants
- Record logs for each plant

---

## Built with

Watering🐛🌳 is built with a number of open source projects to work properly:

### Base languages
* [TypeScript](https://www.typescriptlang.org/)
* [Node.js](https://nodejs.org/en/)
* [JavaScript](https://developer.mozilla.org)

### Web
* [React](https://reactjs.org/)
* [Semantic UI](https://semantic-ui.com/)
* [Google OAuth Login](https://developers.google.com/identity/protocols/oauth2)

### Server(API)
* [Express](https://expressjs.com/)
* [node-postgres](https://github.com/brianc/node-postgres)

---

## Setup
1. Clone this repo and run `yarn install` on each root directory of `api` and `client` to install the required dependencies for each app.
2. To start the apps, run `yarn start` on each root directory. You can access it at `localhost:3000` for `client` and `localhost:5000` for `api`🎈

---