import { diaryQuery } from '../db/queries';
import { execute as pgExecute } from '../db/postgres';
import { IDiary } from '../types';

const diaryService = {
    getAll: async (userId: string, plantId: string) => {
        const query = diaryQuery.selectList(userId, plantId);
        const pgResult = await pgExecute(query) as IDiary[];
        return pgResult;
    },
    add: async (diary: IDiary) => {
        const query = diaryQuery.insert(diary);
        await pgExecute(query);
    },
    remove: async (id: string, userId: string) => {
        const query = diaryQuery.delete(id, userId);
        await pgExecute(query);
    }
}

export default diaryService;