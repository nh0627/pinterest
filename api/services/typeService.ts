import { colorQuery, categoryQuery } from '../db/queries';
import { execute as pgExecute } from '../db/postgres';
import { IType } from '../types';

const typeService = {
    getAll: async (type: string) => {
        const typeQuery = (type === 'color') ? colorQuery : categoryQuery;
        const query = typeQuery.selectList();
        const pgResult = await pgExecute(query) as Array<IType>;
        return pgResult;
    }
}

export default typeService;