import { plantQuery } from '../db/queries';
import { execute as pgExecute } from '../db/postgres';
import { IPlant } from '../types';

const plantService = {
    getAll: async (userId: string) => {
        const query = plantQuery.selectList(userId);
        const pgResult = await pgExecute(query) as Array<IPlant>;
        return pgResult;
    },
    getOne: async (id: string, userId: string) => {
        const query = plantQuery.selectOne(id, userId);
        const pgResult = await pgExecute(query) as Array<IPlant>;
        const plant = pgResult[0] as IPlant;
        return plant;
    },
    add: async (plant: IPlant) => {
        const query = plantQuery.insert(plant);
        await pgExecute(query);
    },
    edit: async (id: string, plant: IPlant) => {
        const query = plantQuery.update(id, plant);
        await pgExecute(query);
    },
    remove: async (id: string, userId: string) => {
        const query = plantQuery.delete(id, userId);
        await pgExecute(query);
    }
}

export default plantService;