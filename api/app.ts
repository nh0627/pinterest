require('dotenv').config();

import express from 'express';
import cors from 'cors';
import { plantRouter, typeRouter, uploadRouter } from './routers';

const PORT_NUM = 5000;

const app = express();
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({
    extended: true
}));

app.use("/public", express.static(__dirname + '/public'));
app.use('/plants', plantRouter);
app.use('/types', typeRouter);
app.use('/upload', uploadRouter);

app.listen(PORT_NUM, () => { console.log('App is listening at ', PORT_NUM) });
