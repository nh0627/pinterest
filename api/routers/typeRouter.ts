import express from 'express';
import { typeService } from '../services';

const router = express.Router();

router.get('/:name', async (request, response) => {
    const { name } = request.params;
    const types = await typeService.getAll(name);
    response.send(types);
});

export default router;