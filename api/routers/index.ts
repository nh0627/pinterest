export { default as plantRouter } from './plantRouter';
export { default as typeRouter } from './typeRouter';
export { default as uploadRouter } from './uploadRouter';