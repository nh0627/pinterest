import express, { Express } from 'express';
import { responseObject as resObj } from '../util';
import { plantService, diaryService } from '../services';

const router = express.Router();

router.get('/', async (request, response) => {
    const { userId } = request.query;
    const plants = await plantService.getAll(userId as string);
    response.send(plants);
});

router.get('/:id', async (request, response) => {
    const { id } = request.params;
    const { userId } = request.query;
    const plants = await plantService.getOne(id, userId as string);
    response.send(plants);
});

router.post('/', async (request, response) => {
    const { body: data } = request;
    await plantService.add(data);
    response.send(resObj);
});

router.put('/:id', async (request, response) => {
    const { id } = request.params;
    const { body: data } = request;
    await plantService.edit(id, data);
    response.send(resObj);
});

router.delete('/:id', async (request, response) => {
    const { id } = request.params;
    const { userId } = request.query;
    await plantService.remove(id, userId as string);
    response.send(resObj);
});

router.get('/:plantId/diaries', async (request, response) => {
    const { plantId } = request.params;
    const { userId } = request.query;
    const diarys = await diaryService.getAll(userId as string, plantId);
    response.send(diarys);
});

router.post('/:plantId/diaries', async (request, response) => {
    const { plantId } = request.params;
    const { body: data } = request;
    const diary = Object.assign(data, { plantId });
    await diaryService.add(diary);
    response.send(resObj);
});

router.delete('/:plantId/diaries/:id', async (request, response) => {
    const { id } = request.params;
    const { userId } = request.query;
    await diaryService.remove(id, userId as string);
    response.send(resObj);
});

export default router;