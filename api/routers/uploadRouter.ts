import express from 'express';
import { upload } from '../util';

const router = express.Router();

router.post('/', function (req, res) {
    upload(req, res, function (err: any) {
        if (err) return res.status(500).json(err);
        return res.status(200).send(req.file);
    })
});
export default router;