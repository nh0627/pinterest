export interface IDefault {
    id: number,
    createdAt?: Date
}

export interface IType extends IDefault {
    name: string
}

export interface IDiary extends IDefault {
    title: string,
    content: string,
    img: string,
    userId: string,
    plantId: number
}

export interface IUser extends IDefault {
    name: string,
    email: string
}

export interface IPlant extends IDefault {
    name: string,
    img?: string,
    description: string,
    startDate: Date,
    lastWateringDate?: Date,
    lastPottingDate?: Date,
    lastFeedingDate?: Date,
    userId: string,
    categoryId: number,
    colorId: number
}

