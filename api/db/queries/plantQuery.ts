import { IPlant } from '../../types';
import { wrapStringWithQuotes } from '../../util';

const plantQuery = {
    selectList: (userId: string) => `
                SELECT 
                    id, name, user_id, color_id, 
                    category_id, img, description, start_date,
                    last_watering_date, last_potting_date, last_feeding_date, created_at
                FROM 
                    plants
                WHERE user_id = '${userId}'
                `
    ,
    selectOne: (id: string, userId: string) => `
                SELECT 
                    id, name, user_id, color_id, 
                    category_id, img, description, start_date,
                    last_watering_date, last_potting_date, last_feeding_date, created_at
                FROM 
                    plants 
                WHERE id = ${id}
                AND user_id = '${userId}'
                `
    ,
    insert: (plant: IPlant) => {
        const { name, img, description, startDate,
            lastWateringDate, lastFeedingDate, lastPottingDate,
            categoryId, userId, colorId } = wrapStringWithQuotes(plant) as IPlant;
        return `
            INSERT INTO public.plants (
                id, name, description, start_date, 
                category_id, user_id, color_id, created_at, img, 
                last_watering_date, last_potting_date, last_feeding_date
            ) VALUES (
                nextval('plants_sequence'), ${(name)}, ${(description)}, 
                ${(startDate)},
                ${categoryId}, ${userId}, ${colorId}, NOW(), ${(img)}, 
                ${(lastWateringDate)}, ${(lastFeedingDate)}, ${(lastPottingDate)}
            )`
    },
    update: (id: string, plant: IPlant) => {
        const { name, img, description, startDate, lastWateringDate, lastFeedingDate, lastPottingDate,
            categoryId, userId, colorId } = wrapStringWithQuotes(plant) as IPlant;
        return `
            UPDATE public.plants
            SET name=${(name)}, img=${(img)}, 
            last_watering_date=${(lastWateringDate)}, 
            last_feeding_date=${(lastFeedingDate)}, last_potting_date=${(lastPottingDate)},
            category_id=${categoryId}, user_id=${userId}, color_id=${colorId}, 
            description=${(description)}, start_date=${(startDate)}
            WHERE id = ${id}`
    },
    delete: (id: string, userId: string) => `DELETE FROM plants WHERE id = ${id} AND user_id = '${userId}'`
};

export default plantQuery;