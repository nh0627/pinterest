import { IDiary } from '../../types';
import { wrapStringWithQuotes } from '../../util';

const diaryQuery = {
    selectList: (userId: string, plantId: string) => `
        SELECT T1.id, title, content, T1.img, T1.created_at, T1.user_id, plant_id
        FROM public.diaries T1
        INNER JOIN plants T2 ON T1.plant_id = T2.id 
        WHERE T1.user_id = '${userId}'
        AND T1.plant_id = ${plantId}
    `
    ,
    insert: (diary: IDiary) => {
        const { title, content, img, plantId, userId } = wrapStringWithQuotes(diary) as IDiary;
        return `
        INSERT INTO public.diaries(
            id, title, content, img, created_at, plant_id, user_id)
            VALUES (nextval('plants_sequence'), ${title}, ${content}, ${img}, NOw(), ${plantId}, ${userId});
    `
    },
    delete: (id: string, userId: string) => `DELETE FROM diaries WHERE id = ${id} AND user_id = '${userId}'`
};

export default diaryQuery;