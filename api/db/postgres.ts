import pg, { Client } from "pg";
const pgCamelcase = require('pg-camelcase');
require('dotenv').config();
pgCamelcase.inject(pg);

const DB_PASSWORD = process.env.DB_PASSWORD;

const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'buutti',
    password: DB_PASSWORD,
    port: 5432,
});

client.connect();

export const execute = async (query: string) => {
    try {
        console.log("Executed query: ", query);
        const { rows } = await client.query(query);
        return rows;
    } catch (err) {
        console.log(err.stack);
    }
}

export const close = () => client.end;