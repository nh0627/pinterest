import multer from 'multer';

export function wrapStringWithQuotes(obj: any) {
    const quoteData = Object.keys(obj).reduce((prev, key) => {
        return (typeof obj[key] === 'string') ? { ...prev, [key]: `'${obj[key]}'` } : { ...prev, [key]: obj[key] }
    }, {});

    return quoteData;
}

export const responseObject = { success: true };

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});

export const upload = multer({ storage: storage }).single('file');