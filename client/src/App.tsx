import { useEffect, useState } from 'react';
import { Container } from 'semantic-ui-react';
import { IPlant, IType, IDiary } from './types';
import api from './api';
import Menu from './components/Menu';
import PlantList from './components/PlantList';
import PlantPanel from './components/PlantPanel';

const App = () => {

  const [modalOpen, setModalOpen] = useState(false);
  const [colors, setColors] = useState<IType[]>([]);
  const [categories, setCategories] = useState<IType[]>([]);
  const [plants, setPlants] = useState<IPlant[]>([]);
  const [selectedPlant, setSelectedPlant] = useState<IPlant>({} as IPlant);
  const [selectedPlantId, setSelectedPlantId] = useState(0);
  const [selectedDiaries, setSelectedDiaries] = useState<IDiary[]>([]);
  const [selectedDiary, setSelectedDiary] = useState<IDiary>({} as IDiary);
  const [imgFile, setImgFile] = useState<File | null>({} as File | null);
  const [userId, setUserId] = useState<string>();
  const [isSignedIn, setIsSignedIn] = useState(false);

  const fileuploadconfig = { headers: { 'content-type': 'multipart/form-data' } };

  const loadPlants = async () => {
    const { data: plantData } = await api('get', '/plants?userId=' + userId);
    setPlants(plantData);
  }

  const loadDiaries = async () => {
    const { data: diaryList } = await api('get', '/plants/' + selectedPlantId + '/diaries?userId=' + userId);
    setSelectedDiaries(diaryList);
  }

  useEffect(() => {
    const initData = async () => {
      const { data: categoryData } = await api('get', '/types/category');
      const { data: colorData } = await api('get', '/types/color');
      setColors(colorData);
      setCategories(categoryData);
    };
    initData();
  }, []);

  useEffect(() => {
    (isSignedIn)
      ? loadPlants()
      : setPlants([]);
  }, [isSignedIn]);

  useEffect(() => {
    if (selectedPlantId > 0) {
      const loadPlant = async () => {
        const { data: plant } = await api('get', '/plants/' + selectedPlantId + '?userId=' + userId);
        setSelectedPlant(plant);
      }
      loadPlant();
      loadDiaries();
    }
  }, [selectedPlantId]);

  const onAdd = () => {
    setModalOpen(true);
    setSelectedPlant({} as IPlant);
    setSelectedPlantId(0);
  }

  const onDelete = async () => {
    await api('delete', '/plants/' + selectedPlantId + '?userId=' + userId);
    setModalOpen(false);
    loadPlants();
  }

  const onDeleteDiary = async (id: number) => {
    await api('delete', '/plants/' + selectedPlant.id + '/diaries/' + id + '?userId=' + userId);
    loadDiaries();
  }

  const onSubmit = async () => {
    uploadFile();
    (selectedPlantId === 0)
      ? await api('post', '/plants/', { ...selectedPlant, userId })
      : await api('put', '/plants/' + selectedPlant.id, selectedPlant);
    loadPlants();
  }

  const uploadFile = async () => {
    if (imgFile) {
      const fileData = new FormData();
      fileData.append("file", imgFile);
      await api('post', '/upload', fileData, fileuploadconfig);
    }
  };

  const onDiarySubmit = async () => {
    uploadFile();
    await api('post', '/plants/' + selectedPlant.id + '/diaries/', { ...selectedDiary, userId });
    setSelectedDiary({ title: '', content: '', img: '', imgFile: undefined } as IDiary);
    loadDiaries();
  }

  return (
    <Container>
      <Menu onAdd={onAdd} setUserId={setUserId} isSignedIn={isSignedIn} setIsSignedIn={setIsSignedIn} />
      <PlantList
        plants={plants}
        setId={setSelectedPlantId}
        setModalOpen={setModalOpen}
      />
      <PlantPanel
        open={modalOpen}
        plant={selectedPlant}
        diary={selectedDiary}
        colors={colors}
        categories={categories}
        diaries={selectedDiaries}
        setPlant={setSelectedPlant}
        setDiary={setSelectedDiary}
        setImgFile={setImgFile}
        setOpen={setModalOpen}
        onSubmit={onSubmit}
        onDiarySubmit={onDiarySubmit}
        onDelete={onDelete}
        onDeleteDiary={onDeleteDiary}
      />
    </Container>
  );
}

export default App;
