import axios, { AxiosRequestConfig } from 'axios';

interface AxiosStatic {
    [key: string]: any;
}

const createdAxios = axios.create({
    baseURL: 'http://localhost:5000'
});

const callAPI = async (method: string, url: string, data?: any, config?: AxiosRequestConfig | undefined) => {
    try {
        const axiosMethod = (createdAxios as AxiosStatic)[method];
        const returnObj = await axiosMethod(url, data, config);
        return returnObj;
    } catch (e) {
        alert("we cannot proceed with this request. Error: " + e);
    }
}

export default callAPI;