import { Button, Modal, Image } from 'semantic-ui-react';
import DiaryList from '../DiaryList';
import { IPlant, IType, IDiary } from '../../types';
import PlantForm from '../PlantForm';

interface IPlantPanel {
    open: boolean,
    plant: IPlant,
    diary: IDiary,
    setOpen: (open: boolean) => void,
    setPlant: (plant: IPlant) => void,
    setImgFile: (file: File | null) => void,
    setDiary: (diary: IDiary) => void,
    onSubmit: () => void,
    onDiarySubmit: () => void,
    onDelete: () => void,
    onDeleteDiary: (id: number) => void,
    colors: IType[],
    categories: IType[]
    diaries: IDiary[]
}

const PlantPanel = (props: IPlantPanel) => {

    const { open, setOpen, plant, setPlant, colors, categories,
        onSubmit, onDelete, diaries, onDeleteDiary, setDiary, diary, onDiarySubmit, setImgFile } = props;

    const IMG_URL = 'http://localhost:5000/public/';

    const close = () => setOpen(false);

    return (
        <Modal
            size='large'
            onClose={close}
            open={open}
        >
            <Modal.Header>🌳{plant?.name}🌸's Log</Modal.Header>
            <Modal.Content image>
                {(plant.img) ? <Image size='medium' src={IMG_URL + plant?.img} wrapped /> : null}
                <Modal.Description>
                    <PlantForm
                        plant={plant}
                        setPlant={setPlant}
                        setImgFile={setImgFile}
                        colors={colors}
                        categories={categories} />
                </Modal.Description>
            </Modal.Content>
            { (plant.id > 0)
                ? <DiaryList diaries={diaries}
                    onDelete={onDeleteDiary}
                    onSubmit={onDiarySubmit}
                    setDiary={setDiary}
                    setImgFile={setImgFile}
                    diary={diary} />
                : null
            }
            <Modal.Actions>
                <Button
                    content="Save"
                    labelPosition='right'
                    icon='checkmark'
                    onClick={() => { close(); onSubmit(); }}
                    positive
                />
                {(plant.id > 0)
                    ? <Button
                        color='red'
                        onClick={onDelete}>
                        Delete
                </Button>
                    : null}
                <Button
                    color='black'
                    onClick={close}>
                    Close
                </Button>
            </Modal.Actions>
        </Modal>
    )
}

export default PlantPanel;