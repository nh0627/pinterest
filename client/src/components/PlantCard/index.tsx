import React from 'react';
import { Card, Image } from 'semantic-ui-react';
import { IPlant } from '../../types';

interface IPlantCard {
    plant: IPlant,
    setId: (id: number) => void,
    setModalOpen: (open: boolean) => void
}

const ImageCard = (props: IPlantCard) => {
    const { plant: { name, img, description, startDate, id }, setId, setModalOpen } = props;
    const IMG_URL = 'http://localhost:5000/public/';

    const onCardClick = (event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        const { id } = event.currentTarget;
        setId(parseInt(id)); 
        setModalOpen(true);
    };

    return (
        <Card onClick={(e, data) => onCardClick(e)} id={id}>
            <Image src={IMG_URL + img} alt={name} wrapped ui={false} />
            <Card.Content>
                <Card.Header>{name}</Card.Header>
                <Card.Meta>
                    <span className='date'>{new Date(startDate).toLocaleDateString()}</span>
                </Card.Meta>
                <Card.Description>
                    {description}
                </Card.Description>
            </Card.Content>
        </Card>
    )
}

export default ImageCard;