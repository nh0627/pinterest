import { useRef } from 'react';
import { FormInputProps } from 'semantic-ui-react';
import DatePicker from 'react-semantic-ui-datepickers';
import { Form } from 'semantic-ui-react';
import { IPlant, IType } from '../../types';

interface IPlantForm {
    plant: IPlant,
    colors: IType[],
    categories: IType[],
    setPlant: (plant: IPlant) => void,
    setImgFile: (file: File | null) => void
}

const PlantForm = (props: IPlantForm) => {

    const { plant, setPlant, setImgFile, colors, categories } = props;

    const fileInputRef = useRef({} as HTMLInputElement);

    const handlePlantChange = (name: string, value: any) => {
        setPlant({ ...plant, [name]: value });
    }

    const handleFileSelected = (files: FileList | null) => {
        const file = (files !== null) ? files[0] : null;
        setImgFile(file);
        setPlant({ ...plant, img: file?.name });
    }

    const getTypes = (type: string) => {
        const types = (type === 'color') ? colors : categories;
        return types.map(type => {
            return {
                key: type.id,
                value: type.id,
                text: type.name
            }
        });
    }

    const { id = "", name = "", description = "", colorId, categoryId,
        startDate = new Date(),
        lastWateringDate = new Date(), lastPottingDate = new Date(),
        lastFeedingDate = new Date() } = plant;

    const renderDatePicker = (args: FormInputProps) => {
        const { label, name, value } = args;

        return (value === undefined) ? null :
            <DatePicker
                label={label}
                name={name}
                value={new Date(value)}
                onChange={(e, data) => handlePlantChange(data.name, data.value)}
            />;
    }

    return (
        <Form>
            <Form.Group widths='equal'>
                <Form.Input
                    label='id'
                    name='id'
                    value={id}
                    disabled={true}
                />
                <Form.Input
                    label='Name'
                    name='name'
                    value={name}
                    onChange={(e) => handlePlantChange(e.target.name, e.target.value)}
                />
                {renderDatePicker({ value: startDate, name: 'startDate', label: 'Start Date' })}
            </Form.Group>
            <Form.Group widths='equal'>
                <Form.Select
                    fluid
                    selection
                    name='categoryId'
                    label='Category'
                    options={getTypes('category')}
                    placeholder='Select category'
                    value={categoryId}
                    onChange={(e, data) => handlePlantChange(data.name, data.value)}
                />
                <Form.Select
                    fluid
                    selection
                    label='Color'
                    name='colorId'
                    options={getTypes('color')}
                    placeholder='Select color'
                    value={colorId}
                    onChange={(e, data) => handlePlantChange(data.name, data.value)}
                />
            </Form.Group>
            <Form.TextArea
                label='Description'
                name='description'
                value={description}
                onChange={(e) => handlePlantChange(e.target.name, e.target.value)}
            />
            <Form.Button
                content="Choose File"
                labelPosition="left"
                icon="file"
                onClick={() => fileInputRef.current.click()}
            />
            <input
                ref={fileInputRef}
                type="file"
                hidden
                onChange={(e) => handleFileSelected(e.target.files)}
            />
            <Form.Group widths='equal'>
                {renderDatePicker({ value: lastWateringDate, name: 'lastWateringDate', label: 'Last Watering Date' })}
                {renderDatePicker({ value: lastPottingDate, name: 'lastPottingDate', label: 'Last Potting Date' })}
                {renderDatePicker({ value: lastFeedingDate, name: 'lastFeedingDate', label: 'Last Feeding Date' })}
            </Form.Group>
        </Form>
    )
}

export default PlantForm;