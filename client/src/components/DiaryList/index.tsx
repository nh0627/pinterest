import { useRef } from 'react';
import { Divider, Card, Modal, Button, Form } from 'semantic-ui-react';
import { IDiary } from '../../types';

interface IDiaryList {
    diaries: IDiary[],
    diary: IDiary,
    setDiary: (diary: IDiary) => void,
    onDelete: (id: number) => void,
    onSubmit: () => void,
    setImgFile: (file: File | null) => void
}

const DiaryList = (prop: IDiaryList) => {
    const { diaries, onDelete, setDiary, diary, onSubmit, setImgFile } = prop;
    const fileInputRef = useRef({} as HTMLInputElement);
    const IMG_URL = 'http://localhost:5000/public/';

    const handleChange = (name: string, value: any) => {
        setDiary({ ...diary, [name]: value });
    }

    const handleFileSelected = (files: FileList | null) => {
        const file = (files !== null) ? files[0] : null;
        setImgFile(file);
        setDiary({ ...diary, img: file?.name });
    }

    const cardButton = (id: number) => (
        <Card.Content extra>
            <Button basic color='red' onClick={() => onDelete(id)}>Delete</Button>
        </Card.Content>
    );

    const renderCard = (diary: IDiary) => {
        const { img, title, content, id } = diary;
        return (
            <Card
                key={id}
                image={IMG_URL + img}
                header={title}
                description={content}
                extra={cardButton(id)}
            />
        );
    };

    return (
        <Modal.Content>
            <Divider />
            <Card.Group itemsPerRow={4}>
                {(diaries.length > 0) ? diaries.map((diary: IDiary) => renderCard(diary)): null}
            </Card.Group>
            <Divider hidden />
            <Form>
                <Form.Input
                    label='Title'
                    name='title'
                    value={diary.title}
                    onChange={(e, data) => handleChange(e.target.name, e.target.value)}
                />
                <Form.TextArea
                    label='Content'
                    name='content'
                    value={diary.content}
                    onChange={(e, data) => handleChange(e.target.name, e.target.value)}
                />
                <Button
                    content="Choose File"
                    labelPosition="left"
                    icon="file"
                    onClick={() => fileInputRef.current.click()}
                />
                <input
                    ref={fileInputRef}
                    type="file"
                    hidden
                    onChange={(e) => handleFileSelected(e.target.files)}
                />
                <Button type='submit' onClick={onSubmit}>Submit</Button>
            </Form>
        </Modal.Content>
    )
}

export default DiaryList;