import PlantCard from '../PlantCard';
import styles from './PlantList.module.css';
import { IPlant } from '../../types';

interface IPlantList {
    plants: IPlant[],
    setId: (id: number) => void,
    setModalOpen: (open: boolean) => void
}

const PlantList = (props: IPlantList) => {
    const { plants = [], setId, setModalOpen } = props;

    const renderPlantCard = (plant: IPlant) => <PlantCard key={plant.id} plant={plant} setId={setId} setModalOpen={setModalOpen} />;

    const renderColumn = (PlantCards: JSX.Element[], i: number) => <div className={styles.column} key={i}>{[...PlantCards]}</div>;

    const maxColumns = 4;

    const renderColumns = (plants: IPlant[]) => {
        const dividedPlantCards: Array<(JSX.Element)[]> = [];
        if (plants.length > 0) {
            plants.forEach((plant, index) => {
                const remainder = index % maxColumns;
                if (dividedPlantCards[remainder] === undefined) dividedPlantCards[remainder] = [];
                dividedPlantCards[remainder].push(renderPlantCard(plant));
            });
        }
        return dividedPlantCards.map((plantCards, i) => renderColumn(plantCards, i))
    }

    return (
        <div className={styles.row}>
            {renderColumns(plants)}
        </div>
    )
}

export default PlantList;