import GoogleLogin, { GoogleLogout, GoogleLoginResponse, GoogleLoginResponseOffline } from 'react-google-login';
import { Button, Icon } from 'semantic-ui-react';

interface ILogin {
    setUserId: (id: string) => void,
    isSignedIn: boolean,
    setIsSignedIn: (isSignedIn : boolean) => void
}

const Login = (props: ILogin) => {

    const { setUserId, isSignedIn, setIsSignedIn } = props;

    const clientId = process.env.REACT_APP_PUBLIC_GOOGLE_CLIENT_ID;

    const onSuccess = (response: GoogleLoginResponse | GoogleLoginResponseOffline) => {
        if ('googleId' in response) {
            const { googleId } = response;
            setUserId(googleId);
            setIsSignedIn(true);
        }
    };

    const onLogoutSuccess = () => {
        setIsSignedIn(false);
        setUserId('');
    }

    const renderButton = (onClick: () => void, disabled: boolean | undefined, color: any, text: string) => (
        <Button
            fluid
            color={color}
            onClick={onClick}
            disabled={disabled}>
            <Icon name='google' /> {text}
        </Button>
    );

    const login = (
        <GoogleLogin
            clientId={clientId as string}
            render={renderProps => renderButton(renderProps.onClick, renderProps.disabled, 'google plus', 'Login with Google')}
            onSuccess={onSuccess}
            onFailure={result => console.log(result)}
            isSignedIn={true}
            cookiePolicy={'single_host_origin'}
        />
    )

    const logout = (
        <GoogleLogout
            clientId="658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com"
            render={renderProps => renderButton(renderProps.onClick, renderProps.disabled, 'black', 'Logout')}
            onLogoutSuccess={onLogoutSuccess}
        />
    )

    return (!isSignedIn) ? login : logout;
}

export default Login;