import { Menu, Button } from 'semantic-ui-react';
import GoogleLogin from '../GoogleAuth';

interface IMenu {
  onAdd: () => void,
  setUserId: (id: string) => void,
  isSignedIn: boolean,
  setIsSignedIn: (isSignedIn: boolean) => void
}

const Header = (props: IMenu) => {
  const { onAdd, setUserId, setIsSignedIn, isSignedIn } = props;

  return (
    <Menu>
      <Menu.Item>
        <Button
          primary
          onClick={onAdd}
        >
          Add
        </Button>
        <GoogleLogin setUserId={setUserId} isSignedIn={isSignedIn} setIsSignedIn={setIsSignedIn} />
      </Menu.Item>
    </Menu>
  )
};

export default Header;